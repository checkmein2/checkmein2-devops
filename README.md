# Common space for everyone to include tips & tricks .

## How to clone all repos in one command :

Mare sure you have added your public key ( ~/.ssh/id_rsa.pub ) in your own profile settings at "SSH KEYS" menu

*Run the following command in terminal* :

```console
list=(checkmein2-challenge-service checkmein2-notification-service checkmein2-ui checkmein2-gamification-service checkmein2-ws checkmein2-campaign-service checkmein2-api-gateway checkmein2-media-service checkmein2-user-management-service checkmein2-mobile checkmein2-activity-service) && for i in "${list[@]}";do echo "[INFO] Cloning repo $i"; git clone git@gitlab.com:checkmein2/$i.git; done
```